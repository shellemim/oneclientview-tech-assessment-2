# OneClientView Technical Assessment
### Author: Michelle Leong Hwee-Ling
## !Important Note!
This application does not strictly follow the assessment requirements (to use GoLang for the backend), as during development I was unable to overcome a critical issue that was preventing me from communicating with my database. Additionally, due to time constraints, I was unable to continue investing time into developing this Rest API in GoLang. However, I have managed to completed the assessment requirements using a Flask API in this repository, complete with CI integration. I hope you will take this work into consideration when evaluating my competency as a candidate in developing Rest APIs.
## Description
This project is an REST API interface, with the aim of providing the backend integration for teachers to access and manage their student's information. This application was developed as part of the GovTech OneClientView Technical Assessment.

As this Rest API is very isolated and lightweight, a single microservice approach was taken. Student data is stored, processed and queries within our students-microservice, which uses a MySQL database, and is containerized and hosted on ECS. This microservices approach improves the functionality of the service by allowing the Rest API to be more easily scalable, more reliable, and have higher availability.

## Technologies Used:
- Flask App for Rest API (Yes, it is noted that this assignment was ment to be in GoLang. However, as explained in the submission email, due to set-up issues and time constraints, flask was used instead. For the GoLang Repository, please refer [here](https://gitlab.com/shellemim/oneclientview-tech-assessment).)
- Docker for containerizing and running the application locally

## File Directory
<!-- trunk-ignore(markdownlint/MD040) -->
```
.
├── services
│   └── students-microservice       # Microservice for managing students
│       ├── src                     # Director holding Main Application source code
│       ├── tests                   # Directory holding Unit Tests Unit Testing Config
│       ├── ci                      # Directory holding CI unit testing integration
│       └── Dockerfile              # Dockerfile for students microservice
├── docker-compose.yml              # Docker Compose yml
└── ...
```
## Usage
### API Docs
For desription of the APIs, you can refer to [the original Assessment Details](https://docs.google.com/document/d/1GIAcPG28lCF-D4LuQjM694fVpFHyBL1Q/edit)

Here are the out of the box APIs: [![Run in Postman](https://run.pstmn.io/button.svg)](https://god.gw.postman.com/run-collection/23052220-714f1790-981a-491a-bcce-1de786b80671?action=collection%2Ffork&collection-url=entityId%3D23052220-714f1790-981a-491a-bcce-1de786b80671%26entityType%3Dcollection%26workspaceId%3De37cffae-d72b-4c85-a627-954307238d53)
### Running Locally
This repository is publicly availble to be cloned and run locally on your own macbine. Please refer to the [Installation](#installation) section for a guide on how to set-up this Rest API on your local environment.

### Database Structure
In order to keep in line with the microservices approach, the database was created to house only student data (Email, Suspended status). However, our requirements dictate that each Student must be associated with one or more teacher. This would require a relational database, however, as we are taking on a microservices approach, we should only deal with one entity per microservice. As such, we use an ID in order to keep our rows unique, as well as verify that no entries exist with the same student email and teacher email when creating new student records.

As such, here is our final database structure:

| **id** | email | suspended | teacher |
| --- | --------- | -------- | --------|
| Integer | String | Boolean | String |

### Unit Tests
Unit tests can be found in /services/students-microservices/tests/test_integration.py. In order to run these rests, please CD to /services/students-microservices in your terminal, and run:
```
docker compose -f ci/docker-compose.test.yml up -d --build
```
This command will run the integration tests within a docker. Open up the docker interface and navigate to the logs in order to view the test results.
## Installation
1. Ensure you have [Docker](https://docs.docker.com/engine/install/) installed
2. CD into the main directory (/oneclientview-tech-assessment)
3. Run "docker compose up -d --build" in the command line
4. App will be run on localhost:30000
## Project Management
Due to the tight timeline of this assessment, it was crucial to use projet management tools to keep the project on track, and ensure no requirements/components of the assessments were missed during the development of the application. As such, a KanBan board was used to keep track of all the tasks to be completed.

![Kanban Board](/readme_res/Kanban.png "Kanban Board")
![Kanban Board Card](/readme_res/Card.png "Kanban Board Card")

During the initial project planning, the tasks identified from the requirements were as follows (in order of planned succession):
1. Create Git Repository & Base Application
2. Dockerization
3. Set up & Connect AWS RDS MySQL Server
4. Develop Rest APIs
5. Write Test Cases
6. Implement automated testing in CI
7. Host Rest API on ECS
8. Write Documentation & Code Clean-Up

However, due to time constraits (having rushed this application in about 4 hours), Hosting was left out from the actual implemetation.