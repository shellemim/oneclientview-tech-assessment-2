import pytest


@pytest.fixture
def client():
    from src import app

    app.app.config['TESTING'] = True

    app.db.engine.execute('DROP TABLE IF EXISTS `students`;')
    
    app.db.engine.execute('''CREATE TABLE students (
    id int NOT NULL AUTO_INCREMENT,
    email varchar(32) NOT NULL,
    suspended int NOT NULL,
    teacher varchar(32) NOT NULL,
    PRIMARY KEY (id)
);''')

    app.db.engine.execute('''INSERT INTO `students` (id, email, suspended, teacher) VALUES (1, 'student1@gmail.com', 0, 'teacher1@gmail.com'),
(2, 'student2@gmail.com', 0, 'teacher1@gmail.com'),
(3, 'student1@gmail.com', 0, 'teacher2@gmail.com'),
(4, 'student3@gmail.com', 1, 'teacher2@gmail.com'),
(5, 'student4@gmail.com', 0, 'teacher2@gmail.com');''')


    return app.app.test_client()