import json
import pytest


def call(client, path, method='GET', body=None):
    mimetype = 'application/json'
    headers = {
        'Content-Type': mimetype,
        'Accept': mimetype
    }

    if method == 'POST':
        response = client.post(path, data=json.dumps(body), headers=headers)
    elif method == 'PUT':
        response = client.put(path, data=json.dumps(body), headers=headers)
    elif method == 'PATCH':
        response = client.patch(path, data=json.dumps(body), headers=headers)
    elif method == 'DELETE':
        response = client.delete(path)
    else:
        response = client.get(path)

    return {
        "json": json.loads(response.data.decode('utf-8')),
        "code": response.status_code
    }


# Test service is healthy
@pytest.mark.dependency()
def test_health(client):
    result = call(client, 'health')
    assert result['code'] == 200


# Test to make sure service can retrieve from db
@pytest.mark.dependency(depends=['test_health'])
def test_get_all(client):
    result = call(client, 'students')
    assert result['code'] == 200


# Test api/register
@pytest.mark.dependency(depends=['test_get_all'])
def test_register(client):
    result = call(client, 'api/register', 'POST', {
        "teacher": "teacherjoe@gmail.com",
        "students": 
        [
            "studentjon@gmail.com",
            "studenthon@gmail.com"
        ]
    })
    assert result['code'] == 201
    assert result['json']['data'] == [
        {
            "email": "studentjon@gmail.com",
            "teacher": "teacherjoe@gmail.com"
        },
        {
            "email": "studenthon@gmail.com",
            "teacher": "teacherjoe@gmail.com"
        }
    ]

# Test api/commonstudents with one teacher
@pytest.mark.dependency(depends=['test_get_all'])
def test_commonstudents_one(client):
    result = call(client, 'api/commonstudents?teacher=teacher1%40gmail.com')
    assert result['code'] == 200
    assert result['json']['students'] == [
		"student1@gmail.com",
		"student2@gmail.com"
	]

# Test api/commonstudents with multiple teachers
@pytest.mark.dependency(depends=['test_get_all'])
def test_commonstudents_mult(client):
    result = call(client, 'api/commonstudents?teacher=teacher1%40gmail.com&teacher=teacher2%40gmail.com')
    assert result['code'] == 200
    assert result['json']['students'] == [
		"student1@gmail.com"
	]

# Test api/commonstudents with when one teacher has not registered
@pytest.mark.dependency(depends=['test_get_all'])
def test_commonstudents_fail(client):
    result = call(client, 'http://localhost:30000/api/commonstudents?teacher=teacherken%40gmail.com&teacher=teacherjoe%40gmail.com')
    assert result['code'] == 400


# # Test api/suspend
# @pytest.mark.dependency(depends=['test_get_all'])
# def test_suspend(client):
#     result = call(client, 'api/suspend', 'POST', {
#         "student": "student4@gmail.com"
#     })
#     # 204 response throw error
#     assert result['code'] == 204


# Test api/retrievefornotifications
@pytest.mark.dependency(depends=['test_get_all'])
def test_notifications(client):
    result = call(client, 'api/retrievefornotifications', 'POST', {
        "teacher": "teacher1@gmail.com",
        "notification": "Hello! @student3on@gmail.com @student1@gmail.com"
    })
    assert result['code'] == 200
    assert result['json']['recipients'] == [
		"student1@gmail.com",
		"student2@gmail.com"
	]