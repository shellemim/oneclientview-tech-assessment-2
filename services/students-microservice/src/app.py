import os

from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

app = Flask(__name__)

db_name = '/students_db'
if os.environ.get('db_conn'):
    app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('db_conn') + db_name
else:
    app.config['SQLALCHEMY_DATABASE_URI'] = None
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['SQLALCHEMY_ENGINE_OPTIONS'] = {'pool_size': 100,
                                               'pool_recycle': 280}

db = SQLAlchemy(app)

CORS(app)


class Student(db.Model):
    __tablename__ = 'students'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(64), nullable=False)
    suspended = db.Column(db.Boolean, nullable=False)
    teacher = db.Column(db.String(64), nullable=False)

    def __init__(self, email, teacher, suspended=False):
        self.email = email
        self.suspended = suspended
        self.teacher = teacher

    def to_dict(self):
        return {
            "email": self.email,
            "teacher": self.teacher
        }


@app.route("/health")
def health_check():
    return jsonify(
            {
                "message": "Students Microservice is healthy.",
                "service:": "games"
            }
    ), 200


@app.route("/students")
def get_all():
    student_list = Student.query.all()
    if len(student_list) != 0:
        return jsonify(
            {
                "data": {
                    "students": [student.to_dict() for student in student_list]
                }
            }
        ), 200
    return jsonify(
        {
            "message": "There are no students in the database yet."
        }
    ), 200


@app.route("/api/register", methods=['POST'])
def register():
    try:
        payload = request.get_json()

        teacher = payload["teacher"]
        students_list = payload["students"]

        new_students = []

        for student in students_list:
            # Check if student exists already
            check_if_student_exists = Student.query.filter_by(email=student).filter_by(teacher=teacher).first()

            # If already exists, do not process request
            if check_if_student_exists:
                return jsonify(
                    {
                        "message": "One or more student(s) have already been registered with this teacher",
                        "data": check_if_student_exists.to_dict()
                    }
                ), 400

            new_student = Student(student, teacher)
            new_students.append(new_student)
            # else, add new student to database
            db.session.add(new_student)

        db.session.commit()

    except Exception as e:
        return jsonify(
            {
                "message": "An error occurred registering the student(s).",
                "error": str(e)
            }
        ), 500

    return jsonify(
        {
            "message": "Students successfully registered.",
            "data": [new_student.to_dict() for new_student in new_students]
        }
    ), 201


@app.route("/api/commonstudents")
def find_common():

    teachers_list = request.args.getlist('teacher')

    try:
        common_student_emails = []

        for teacher in teachers_list:
            # Check if student exists already
            students_under_teacher = Student.query.filter_by(teacher=teacher).all()

            if students_under_teacher:
                student_emails = []

                for student in students_under_teacher:
                    student_emails.append(student.email)

                if len(common_student_emails) == 0:
                    common_student_emails = student_emails
                else:
                    repeated_students = set(common_student_emails).intersection(student_emails)
                    common_student_emails = list(repeated_students)
            else: 
                    return jsonify(
                        {
                            "teacher": teacher,
                            "message": "No students registered under this teacher.",
                        }
                    ), 400

        # If already exists, do not process request
        if len(common_student_emails) != 0:
            return jsonify(
                {
                    "students": [email for email in common_student_emails]
                }
            ), 200

    except Exception as e:
        return jsonify(
            {
                "message": "An error occurred while retrieving the student(s).",
                "error": str(e)
            }
        ), 500

    return jsonify(
        {
            "message": "No students/common students found under teacher/s.",
        }
    ), 400


@app.route("/api/suspend", methods=['POST'])
def suspend_student():
    payload = request.get_json()

    student_email = payload["student"]

    suspend_student_list = Student.query.filter_by(email=student_email).all()
    if suspend_student_list is None:
        return jsonify(
            {
                "student": student_email,
                "message": "This student has yet to be registered."
            }
        ), 400

    try:
        for student in suspend_student_list:
            student.suspended = True
            db.session.merge(student)
            db.session.commit()

        updated_suspend_student_list = Student.query.filter_by(email=student_email).all()

    except Exception as e:
        return jsonify(
            {
                "message": "An error occurred while updating suspend status.",
                "error": str(e)
            }
        ), 500

    return jsonify(
        {
            "updated": [student.to_dict() for student in updated_suspend_student_list]
        }
    ), 204


@app.route("/api/retrievefornotifications", methods=['POST'])
def notify_students():
    payload = request.get_json()

    teacher = payload["teacher"]
    notification = payload["notification"]

    # Collect all students registered under that teacher, as well as pinged students
    students_under_teacher = Student.query.filter_by(teacher=teacher).all()
    pinged_students = [word for word in notification.split() if word.startswith('@')]

    student_emails = []

    try:
        # Add students who are registered under this teacher
        if students_under_teacher:
            for student in students_under_teacher:
                if not student.suspended:
                    student_emails.append(student.email)

        # Add students who were pinged
        if len(pinged_students) != 0:
            for student in pinged_students:
                studen_email = student[1:]

                # Check if student exists in the database
                check_if_exists = Student.query.filter_by(email=studen_email).first()
                if check_if_exists:
                    if not check_if_exists.suspended:
                        student_emails.append(studen_email)

        # Remove duplicates
        student_emails = list(dict.fromkeys(student_emails))

        if len(student_emails) != 0:
            return jsonify(
                {
                    "recipients": [email for email in student_emails]
                }
            ), 200

    except Exception as e:
        return jsonify(
            {
                "message": "An error occurred registering the student(s).",
                "error": str(e)
            }
        ), 500

    return jsonify(
        {
            "message": "No students registered under this teacher.",
        }
    ), 400


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
